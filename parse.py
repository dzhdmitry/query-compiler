from rpn import make, operators, StringInvalid
from tree import *


def build(query):
	"""
	query - string like "(A+B),(C-D)"
	Returns Node (tree of operands and operations)
	"""
	rpn_array = make(query)
	stack = []

	for elem in rpn_array:
		if elem in operators:
			b = stack.pop()
			a = stack.pop()

			if elem == '+':
				c = And([a, b])
			elif elem == '-':
				c = And([a, Not(b)])
			elif elem == ',':
				c = Or([a, b])
			else:
				raise StringInvalid

			stack.append(c)
		else:
			stack.append(Operand(elem))

	if len(stack) > 0:
		return Root(stack.pop())
	else:
		raise StringInvalid


def recognize(tag_string, query):
	tag_string_tree = build(tag_string)
	tag_string_tree.toDNF()

	query_tree = build(query)
	query_tree.toDNF()

	result = False
	tag_string_array = tag_string_tree.toArray()

	#tag_string_tree.print_all()
	#print '---'
	#query_tree.print_all()

	for conjunction in query_tree.toArray():
		elements = {'+': [], '-': []}
		matches = {'+': True, '-': True}

		def find(key):
			for element in elements[key]: # scanning Not-s from query and tags
				found = False

				for tag in tag_string_array:
					found = found or (element == tag[0]) # if _one of_=True => found=True to the end

				matches[key] = matches[key] and found # if _one of_ found=False => matches[-]=False to the end

		for element in conjunction:
			if isinstance(element, Operand):
				elements['+'].append(element)
			elif isinstance(element, Not):
				elements['-'].append(element)

		if not len(elements['-']):
			matches['-'] = False
		else:
			find('-')

		if not matches['-']:
			find('+')

			if matches['+']:
				result = True
				break

	return result


#print recognize('a', 'a')
#print recognize('a,b,c,d', '(a+b),(c+d)')
#print recognize('a,b,c,d', 'a+b+c-k')
#print recognize('a,b,c', '(a,d)+(b-g)')
#print recognize('a,b,c', '(a,b)-k')

#print recognize('a,b,c', '(a,b)+k')
#print recognize('a,b,c,d,e,f,g,h,i', '(a,(b-c))+(d,e)')

#make_query_arrays('a')
#make_query_arrays('a+b')
#make_query_arrays('(a+b),c')
#make_query_arrays('(a,b)+c')

#make_query_arrays('d,((a+b+v)-c)')

#make_query_arrays('(c+(a,b))-d')

#make_query_arrays('(a,b)+(c,d)')

#make_query_arrays('(a,(b+c+n))-d')

#make_query_arrays('(a+g),b,c,d')
#make_query_arrays('a+b+c+d+e')
#make_query_arrays('a,b,c,d,e,f')
#make_query_arrays('(a-(d,c,h))+x')

#make_query_arrays('a-(b+c+d)')