special_symbols = ['+', '-', ',', '(', ')']
operators = ['+', '-', ',']
quotes = ['"', "'"]
brackets = ['(', ')',]


def clear(rpn_item):


	return rpn_item.strip()


class StringInvalid(Exception):
	def __init__(self, *args, **kwargs):
		super(StringInvalid, self).__init__(*args, **kwargs)


def make(string):
	result = [] # goes to out
	symbols = [] # symbols to keep in memory
	current_operand = '' # chars appends here, then it goes to result
	ESCAPE = False
	current_level = 0 # k , ( a + n + ( b - c ))
				#   first^   ^second   ^third
	levels = {
		'0': 0,
		'1': 0,
		'2': 0,
		'3': 0,
		'4': 0
	}

	for offset, char in enumerate(string.strip()):
		if (char not in special_symbols or ESCAPE) and char not in quotes: # part of operand
			if not (current_operand == '' and char == ' '):
				current_operand += char
		elif char in quotes: # one of: ' "
			ESCAPE = not ESCAPE
		else: # special symbol
			if current_operand != '':
				result.append(current_operand)
				current_operand = ''

			if char in operators: # one of: + - ,
				symbols.append(char)
				levels[str(current_level)] += 1
			elif char in brackets: # one of: ( )
				if char == ')':
					for i in range(levels[str(current_level)]):
						result.append(symbols.pop())

					levels[str(current_level)] = 0
					current_level -= 1
				elif char == '(':
					current_level += 1

	if current_operand != '':
		result.append(current_operand)

	for i in range(len(symbols)):
		result.append(symbols.pop())

	return map(lambda x: clear(x), result)


#print compile('a+b')
#print compile('d,((a+b)-c)')
#print compile('(a-(b+c)-t)+g')
#print compile(' afk -  (  assln   +"aui++hv -al" +00) -(skmv+(qwer1,qwer2))')