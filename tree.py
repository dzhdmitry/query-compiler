class Node():
	def processAnd(self):
		if hasattr(self, 'children'):
			for i in range(len(self.children)):
				if isinstance(self.children[i], And):
					_And = self.children[i]
					firstIsOr, secondIsOr = isinstance(_And.children[0], Or), isinstance(_And.children[1], Or)

					if firstIsOr and secondIsOr:
						and_array = []

						for f_child in _And.children[0].children:
							for s_child in _And.children[1].children:
								and_array.append(And([f_child, s_child]))

						self.children[i] = Or(and_array)
					elif firstIsOr or secondIsOr:
						x = 0 if firstIsOr else 1
						y = (x*(-1))+1

						firstOrChild = _And.children[x].children[0]
						secondOrChild = _And.children[x].children[1]
						andChild = _And.children[y]
						self.children[i] = Or([And([andChild, firstOrChild]), And([andChild, secondOrChild])])
					else:
						self.children[i].processAnd()
				else:
					self.children[i].processAnd()

	def processNot(self):
		if hasattr(self, 'children'):
			new_children = []

			for child in self.children:
				if isinstance(child, Not):
					if isinstance(child.children[0], Or):
						orChildren = []

						for ch in child.children[0].children:
							orChildren.append(Not(ch))

						new_children.extend(orChildren)
					elif isinstance(child.children[0], And):
						pass
						print 'oh no'
					else: # Operand
						new_children.append(Not(child.children[0]))
				else:
					child.processNot()
					new_children.append(child)

			self.children = new_children

	def compactAnd(self):
		if hasattr(self, 'children'):
			for child in self.children:
				child.compactAnd()

	def compactOr(self):
		if hasattr(self, 'children'):
			for child in self.children:
				child.compactOr()

	def compactNot(self):
		if hasattr(self, 'children'):
			for child in self.children:
				child.compactNot()


class Root(Node):
	def __init__(self, child):
		self.children = [child]

	def print_all(self):
		for child in self.children:
			child.print_all()

	def processNot(self):
		if isinstance(self.children[0], And):
			others = []
			new_children = []
			operand_found = False

			for child in self.children[0].children:
				if isinstance(child, Not) and isinstance(child.children[0], And):
					new_children = child.children[0].children
					operand_found = True
				else:
					child.processNot()
					others.append(child)

			if operand_found:
				not_s = []

				for ch in new_children:
					not_s.append(And(others + [Not(ch)]))

				self.children[0] = Or(not_s)
			else:
				self.children[0].processNot()
		else:
			self.children[0].processNot()

	def toDNF(self):
		for i in range(4):
			self.processAnd()

		for i in range(4):
			self.compactAnd()

		for i in range(4):
			self.compactOr()

		for i in range(4):
			self.processNot()

	def toArray(self):
		if isinstance(self.children[0], Or):
			result = self.children[0].toArray()
		elif isinstance(self.children[0], And):
			result = [self.children[0].children]
		else: # Operand
			result = [self.children]

		return result


class Operand(Node):
	def __init__(self, value):
		self.value = value

	def __repr__(self):
		return '"%s"' % self.value

	def __eq__(self, other):
		if isinstance(other, Operand):
			return self.value == other.value
		else:
			return False

	def print_all(self, offset=''):
		print offset, '"%s"' % self.value


class Operation(Node):
	def __init__(self, name):
		self.name = name
		self.children = []

	def __repr__(self):
		return self.name

	def print_all(self, offset=''):
		print offset, self.name

		for child in self.children:
			child.print_all(offset + '  ')


class And(Operation):
	def __init__(self, children):
		Operation.__init__(self, 'And')
		self.children = children

	def compactAnd(self):
		new_children = []

		for child in self.children:
			if isinstance(child, And):
				new_children.extend(child.children)
			else:
				new_children.append(child)

		for child in new_children:
			child.compactAnd()

		self.children = new_children


class Or(Operation):
	def __init__(self, children):
		Operation.__init__(self, 'Or')
		self.children = children

	def toArray(self):
		result = []

		for child in self.children:
			if isinstance(child, And):
				result.append(child.children)
			else:
				result.append([child])

		return result

	def compactOr(self):
		new_children = []

		for child in self.children:
			if isinstance(child, Or):
				new_children.extend(child.children)
			else:
				new_children.append(child)

		self.children = new_children


class Not(Operation):
	def __init__(self, child):
		Operation.__init__(self, 'Not')
		self.children.append(child)

	def __repr__(self):
		if len(self.children) == 1:
			return 'Not(%s)' % self.children[0]
		else:
			return Operation.__repr__(self)

	def __eq__(self, other):
		if isinstance(other, Operand):
			return self.children[0] == other
		else:
			return False